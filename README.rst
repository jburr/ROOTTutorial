~~~~~~~~~~~~~
ROOT Tutorial
~~~~~~~~~~~~~

.. contents:: *Table Of Contents*

============
Introduction
============

This tutorial is designed to be an introduction to the `ROOT data processing framework <https://root.cern.ch/>`_.

What this tutorial is *not*:

- A C++/python tutorial.
  I will try to use simple code where possible, and provide examples in both language where practical, but a certain level of familiarity is assumed.
- An exhaustive list of of commands and/or classes.
  The ROOT repository is huge but the classes and their interfaces are usually well described in the `ROOT documentation`_.

What this tutorial *should* do is give you the base level of familiarity necessary to get started.
One of the most useful things to learn from this tutorial is what you need to put into google to find answers to any future problems you may have!

ROOT also provides a `list of tutorials <https://root.cern/doc/master/group__Tutorials.html>`_ and a `'How to' <https://root.cern.ch/howtos>`_ page with many FAQs.
Both are good resources for further reading.
You may also find that others have asked the same question in the past on the `ROOT forum <https://root-forum.cern.ch/>`_.

There is also a list of common problems you may encounter.
At the moment this is fairly sparse, however as you come across problems that aren't documented here

#. Check the ROOT forum/Google to see if there's a simple solution to the problem
#. If that fails, ask your supervisor or me directly
#. If you think it's a problem that other students could run into, ask me to add the problem and solution to this page.

Preparation
===========

The intention is for you to run through the tutorial on the NAF.
Before you arrive you should already have a login for the NAF and for CERN's gitlab service.
You should also ensure that you can log into the NAF with X-forwarding properly set.
I use ``<NAF_USER>`` to refer to your NAF user name in what follows.
You should replace them with your own user name (for example for me ``kinit <NAF_USER>@DESY.DE`` becomes ``kinit burrjona@DESY.DE``).

.. code:: bash

  # Set up a kerberos ticket for the NAF
  kinit <NAF_USER>@DESY.DE
  # SSH into one of the NAF nodes with X forwarding enabled
  ssh -Y <NAF_USER>@naf-atlas.desy.de
  # This is a silly little program that creates a pair of googly eyes to follow
  # your mouse around the screen. However it's a decent test that your X
  # forwarding is working
  xeyes

You should then exit the window that pops up to continue.
If no window appears please ensure that you have X-forwarding enabled.
For some operating systems this requires having a separate program launched (e.g. XMing for windows).

Still logged into the NAF, make some changes to the ``~/.bash_profile`` file.
This is the file repsonsible for setting up your bash shell at each log in.
These changes ensure that you can easily use the ATLAS setup scripts.
Add the following lines

.. code:: bash

  export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
  alias setupATLAS="source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh"

then either log out and back in again or source the script to get those changes in your terminal.

Getting Started
===============

Then you should log in and setup a ROOT version as follows

.. code:: bash

  # Set up the ATLAS coding environment
  setupATLAS
  # Set up a specific ROOT version
  lsetup "root 6.16.00-x86_64-slc6-gcc62-opt" 
  
You should create a dedicated directory to run the tutorial from.
In general I will assume that you're operating from within that directory.

The input file for the tutorial is located at ``/afs/desy.de/user/b/burrjona/public/ROOTTutorial/input.root``.
I suggest you either copy or symlink it to your local directory.
The rest of the tutorial will assume that this is available locally as just ``'input.root'``.

You *can* install ROOT on your laptop rather than working on the NAF directly.
However, this takes too long to do than we have time for during the tutorial.
If you already have ROOT on your laptop feel free to use that rather than running on the NAF.

In order to run the example solutions to the exercises here you can clone the repository.
You will need to have called ``kinit <CERN USER>@CERN.CH`` first.

.. code:: bash

  git clone https://:@gitlab.cern.ch:8443/jburr/ROOTTutorial.git

You can also copy it from my public area

.. code:: bash

  cp -rt . /afs/desy.de/user/b/burrjona/public/ROOTTutorial/ROOTTutorial

All the example solutions require you to run them in the same directory as the ``input.root`` file.

Opening a file in the TBrowser
==============================

The first step is to open up the input file and take a look.
ROOT provides a browser that allows easy inspection of its files - it's a very useful tool so it's a good idea to get used to it.

.. code:: bash

  # open up a ROOT session with our input file loaded in automatically
  # if you add the '-l' flag then ROOT will not display the annoying splash
  # screen
  root input.root
  # Then from inside the session, open a browser
  TBrowser tb

The TBrowser is fairly simple to use and behaves how you would expect a browser to.
Take a few minutes to explore the file and its contents.
When you are done, type ``.q`` to exit ROOT.
If ROOT crashes then the standard ``Ctrl+c`` linux command should work.

Writing ROOT scripts
====================

ROOT is written in C++ and is generally used through compiled C++ scripts but bindings exist for other languages such as python or R.
It is also possible to run ROOT through intepreted C++ macros (similar to just running commands from inside the ROOT intrepreter) but I do not recommend this appraoch.

For this tutorial I will provide most of the code instructions in python, however I will also include C++ versions of all the examples in the `examples/Cpp <examples/Cpp>`_ folder.

========
Tutorial
========

Reading objects from a file
===========================

Create a new script called ``test.py`` containing

.. code:: python

  # Import all the ROOT classes
  import ROOT
  # Open the input file
  f = ROOT.TFile("input.root")
  # Retrieve one of the histograms
  h = f.Get("Histograms/Jets/jetPt")
  # Draw it to a canvas
  h.Draw() 

Now run it using ``python -i test.py``.
Note the ``-i`` flag, without this the canvas won't stay open long enough for you to see it.
You should see a canvas containing the histogram.

Exit the python session with ``exit()``.

Draw a histogram as a PDF
=========================

We are first going to adapt our script so that it prints the histogram to a PDF file rather than displaying it to the screen.

.. code:: python

  import ROOT
  # Tell ROOT not to render the canvas on the screen
  # This is optional but it helps:)
  ROOT.gROOT.SetBatch(True)
  f = ROOT.TFile("input.root")
  h = f.Get("Histograms/Jets/jetPt")
  # Create a canvas object to hold the histogram
  c = ROOT.TCanvas()
  # Draw the histogram
  h.Draw()
  # Save the histogram to a file
  c.Print("jetPt.pdf")

You can then run this script (without the ``-i`` flag) and you should get a PDF file created for you which you can open using the ``display`` command.

The ``Draw`` command is very versatile and can take numerous options as its argument which change what is displayed.
The options for histograms are described in the documentation of the `THistPainter <https://root.cern/doc/v616/classTHistPainter.html>`_ class.

Exercise 1
----------

#. Modify your script to draw the histogram with error bars
#. Try to change the colour of the lines to be red. **Hint:** histograms inherit from the `TAttLine <https://root.cern/doc/v616/classTAttLine.html>`_ class.

The histogram classes also can set attributes on the fill or marker styles.
See if you can figure out how to change these from the TH1 class documentation (**Hint:** find the TH1 documentation of Google and search for 'TAtt').

Creating a new histogram
========================

Now we are going to see how to make a new histogram.

A first comment is that there are several different types of histogram defined by ROOT.
First, there are 1, 2 and 3 dimensional histograms defined (TH1, TH2 and TH3).
Second, each histogram class is further specialised to describe which data type is used to describe the bin contents (TH1I, TH1F, etc).
For almost all use cases it's sufficient to use floats.

When creating a histogram you have to give it a name, a title and information on the bins.
The constructor that we'll use creates a number of bins equally spread from a minimum value to a maximum.

.. code:: python

  h = ROOT.TH1F("h_rand", "Random Number;x;Number of Entries", 30, -3, 3)

This creates a new histogram called 'h_rand' with 30 bins between -3 and 3.
The title string gets split on ``;`` characters with each successive strings being assigned to the histogram title, the x-axis title, the y-axis title and the z-axis title (if present) respectively.

To add entries into this histogram use the ``Fill`` function.
The value passed to this function will be compared to the bins and the contents of the relevant bin will be incremented.

Exercise 2
----------

#. Write a script to create a histogram and fill it with 500 Gaussian distributed random numbers.
   Print it to a pdf file with errors displayed.
   Random numbers can be generated using the `Gaus <https://root.cern.ch/doc/v616/classTRandom.html#a0e445e213eae1343b3d22086ecb87314>`_ method of the TRandom class.
   An instance of this class can be accessed as ``ROOT.gRandom``.
#. Use the TH1 documentation to find out how to add a weight to each fill.
   Use this to apply a uniform weight of 2 to your histogram and see how it looks different to before.

Writing a histogram to a ROOT file
==================================

To create a new ROOT file we use the same constructor as before, except now we need to give an extra option to tell ROOT to make the new file.

.. code:: python

  fOut = ROOT.TFile.Open("output.root", "RECREATE")

``"RECREATE"`` tells ROOT to make a new file, overwriting an old one if it existed.

Other options for this are

``"READ"``:
  Open an existing file - this is the default option we have been using so far

``"CREATE"``
  Creates a new file that doesn't already exist - this will fail if the file exists.

To write the histogram into this file call

.. code:: python

  fOut.WriteTObject(h)

Exercise 3
----------

#. Modify your script from the previous example to write the histogram to a ROOT file
3. Use the TBrowser to check that it is there

Reading data from a TTree
=========================

TTrees are the main method of data storage in LHC experiments.
Each 'entry' of a TTree corresponds to a physics event (e.g. a bunch crossing in LHC data).
Each entry consists of different types of information stored in 'branches'.
For example the transverse momenta of the electrons in your event could be one branch ('ElectronPt' in the input file).

Using TTree::Draw
-----------------

Open a python session and retrieve the input tree from the file

.. code:: python

  import ROOT
  f = ROOT.TFile("input.root")
  t = f.Get("AnalysisTree")

Histograms can be drawn directly from a TTree using the `Draw <https://root.cern.ch/doc/v616/classTTree.html#a73450649dc6e54b5b94516c468523e45>`_ function.

For example try some of the following commands - try to work out what each of them is doing

.. code:: python

  t.Draw("JetPt")
  t.Draw("JetPt[0]")
  t.Draw("JetPt", "fabs(JetEta < 1)")
  t.Draw("EleEta:ElePhi", "ElePt", "COLZ")
  t.Draw("Length$(JetPt)")

You can also pipe the results of the draw command into a histogram.

.. code:: python

  h = ROOT.TH1F("jetPt", ";p_{T} (jet) [MeV]", 100, 0, 200e3)
  t.Draw("JetPt >> jetPt")

Reading branches in a loop
--------------------------

**NOTE:** This is one of the areas that is *very* different between python and C++.
This section will cover the way this works in python, if you are using C++ look at the `Differences between python and C++`_ section.
Looping is much faster in C++ so if you are processing large trees it is better to use C++.

For most analyses TTree::Draw is not sufficient: maybe more complicated calculations must be performed on the data in the Tree or a large number of histograms must be filled.
For these purposes it is necessary to read the information out event-by-event.

In python branches are accessed as attributes directly from the TTree object by name.
First the relevant TTree entry should be loaded using the ``GetEntry`` function, then the variables can be processed.

For example, to fill a histogram with the jet pT variable and another with the electron pT

.. code:: python

  # Prepare the histogram to fill
  h_jetPt = ROOT.TH1F("jetPt", ";p_{T} (jet) [MeV]", 100, 0, 200e3)
  h_elePt = ROOT.TH1F("elePt", ";p_{T} (electron) [MeV]", 100, 0, 200e3)
  # GetEntries returns the total number of entries in the tree
  for iEntry in range(t.GetEntries() ):
    # Now fill the histogram from the value from the TTree
    for pt in t.JetPt:
      h_jetPt.Fill(pt)
    for pt in t.ElectronPt:
      h_elePt.Fill(t.pt)

Exercise 4
----------

#. Write a script to reproduce the histograms from the input ROOT file

==================================
Differences between python and C++
==================================

Compiling ROOT in a C++ program
===============================

Unlike python, C++ has to be compiled.
When including an external library like ROOT, the compiler needs to know where to find its header files and precompiled libraries.
To enable this ROOT provides a command line program ``root-config``.
To compile a single ``.cxx`` file linked against ROOT call

.. code:: bash

  g++ /path/to/file.cxx `root-config --cflags --glibs`

However, as projects get larger and more complicated, or you simply just have to compile the same code several times it becomes easier to use another utility to compile the program.
A good choice is ``CMake`` which is configured using ``CMakeLists.txt`` files.
The C++ examples in this package can be compiled using the `configuration file provided <examples/Cpp/CMakeLists.txt>`_.
In order to use this you will need to get a newer version of CMake from the atlas software

.. code:: bash

  lsetup cmake

It's generally good idea to build projects in a separate directory to the one containing the source files.
Therefore create a separate ``build`` directory (I will assume that it is in the same place as your ``input.root`` file but this isn't required).

.. code:: bash

  mkdir build
  cd build
  cmake ../path/to/examples/Cpp
  make

  cd -
  # For example, run the first example solution.
  ./build/ex01-draw-hist


Functions taking pointers as input
==================================

In the C++ interface many functions take pointers as inputs.
For example, the histogram constructor that takes an array of bins receives this as a pointer.
In python this pointer is replaced by a python ``array``.

The C++ code

.. code:: cpp

  std::vector<float> bins{0, 20e3, 40e3, 60e3, 80e3, 100e3};
  TH1F h("jetPt", ";p_{T} (jet) [MeV]", 100, 5, bins.data() );

in python would be

.. code:: python

  from array import array
  bins = array('f', [0, 20e3, 40e3, 60e3, 80e3, 100e3])
  h = ROOT.TH1F("jetPt", "p_{T} (jet) [MeV]", 100, 5, bins)

Other functions take pointers or references to single variables, for example to output a quantity.
For example the TGraph `GetPoint <https://root.cern.ch/doc/master/classTGraph.html#aef2de9b0f38f49a21b4a0d2b422a648b>`_ function receives two references which will be filled with the x and y coordinates of the requested point.
These can be replaced with instances of the ``ROOT.Double`` class.

The C++ code (where :code:`g` is a TGraph)

.. code:: cpp

  double x;
  double y;
  g.GetPoint(iPoint, x, y);

in python would be

.. code:: python  

  x = ROOT.Double()
  y = ROOT.Double()
  g.GetPointer(iPoint, x, y)

A fuller list can be taken from the ``ctypes`` library (e.g. :code:`from ctypes import c_float`)

Reading objects from TFiles
===========================

Unlike python, C++ is a strongly typed language, which means that the compiler needs to know the type of every variable.
However, the type of an object to be read from a file is only known at run time so you must specify it properly.

This can be done either with a cast

.. code:: cpp

  TFile f("input.root");
  TH1F* h = dynamic_cast<TH1F*>(f.Get("Histograms/Jets/jetPt") );

Or by passing in a pointer to the `GetObject <https://root.cern.ch/doc/master/classTDirectory.html#ae8a500db9621fcad6a6f00afebcbcf72>`_ function

.. code:: cpp

  TFile f("input.root");
  TH1F* h;
  f.GetObject("Histograms/Jets/jetPt", h);

Reading data from a TTree
=========================

There are a few ways to read from a TTree in C++ (which can also be used in python, though they are not as easy to use as the by-attribute method shown before).

This version uses the `TTreeReader <https://root.cern.ch/doc/v608/classTTreeReader.html>`_ class.
The TTree is loaded into a TTreeReader object, then any branches are declared using TTreeReaderValues and TTreeReaderArrays and linked to the reader.
Then entries are looped through using the :code:`Next` function and the values stored accessed through the TTreeReaderValue `dereference operator <https://root.cern.ch/doc/v608/classTTreeReaderValue.html#ae461e7bf6834322d56572b3be5c04a6c>`_ or the TTreeReaderArray `At function <https://root.cern.ch/doc/v608/classTTreeReaderArray.html#a07741f78241ea91b14ebd71f4ebdd2c3>`_, :code:`operator[]`  or iterator access).

.. code:: cpp

  TFile f("input.root")
  TTree* t;
  f.GetObject("AnalysisTree", t);
  TTreeReader reader(t);
  TTreeReaderArray<float> jetPt(reader, "JetPt");
  TH1F h("jetPt", ";p_{T} (jet) [MeV]", 100, 0, 200e3);
  while (reader.Next() ) {
    for (float pt : jetPt)
      h.Fill(pt);
  }

===============
Common Problems
===============

ROOT deletes your histogram
===========================

ROOT maintains its own internal list of many objects, such as histograms and trees.
These objects are typically associated to a 'directory', for example an open TFile.
ROOT sees these objects as 'owned' by that directory - when the directory is closed the histograms are deleted.

ROOT keeps a note of the 'current' directory, typically the last TFile or TDirectory opened and any new histograms will **automatically** be put in this directory.
This means that the following code

.. code:: python

  # In 
  fp = ROOT.TFile("test-file.root", "RECREATE")
  h = ROOT.TH1F("test_hist", "", 10, 0, 10)
  fp.Close()
  print( h.GetName() )

results in the error message

.. code:: bash

  AttributeError: 'PyROOT_NoneType' object has no attribute 'GetName'

(In C++ similar code would cause a seg-fault as you tried to access a nullptr.)
In this code the directory being opened and closed is rather obvious, however it can sometimes be hidden behind multiple function calls etc. making this rather hard to debug.
You can change the directory a histogram is associated to using the `TH1::SetDirectory <https://root.cern.ch/doc/master/classTH1.html#a0367fe04ae8709fd4b82795d0a5462c4>`_ function.
Passing a ``nullptr`` into this function means that the histogram is not associated to any directory, which is often the desired behaviour.

This problem does not just manifest itself with errors and segfaults - sometimes it results in your object being quietly removed from your ``TCanvas`` and therefore giving you a completely empty plot!

Python deletes your histogram
-----------------------------

Python **also** has its own garbage collection mechanism which can occasionally disagree with ROOT's.
Python can delete objects that ROOT is still using.
For example the following code

.. code:: python

  c = ROOT.TCanvas()
  def draw_hist()
    h = ROOT.TH1F("my_hist", "Hist", 10, 0, 10)
    h.Draw()
  
  draw_hist()
  c.Print("test.pdf")

Results in a completely empty pad being written to ``test.pdf``.
If the histogram is returned from the function however

.. code:: python

  c = ROOT.TCanvas()
  def draw_hist()
    h = ROOT.TH1F("my_hist", "Hist", 10, 0, 10)
    h.Draw()
    return h
  
  h = draw_hist()
  c.Print("test.pdf")

Then ``test.pdf`` will contain the histogram's axes as expected.
An alternative is to directly disable Python's garbage collection for that object using

.. code:: python

  ROOT.SetOwnership(h, False)

.. _ROOT documentation: https://root.cern/doc/v616/
