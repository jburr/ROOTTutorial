#include <TH1.h>
#include <TFile.h>
#include <TRandom.h>

int main () {
  TH1F h("h_rand", "Random Number;x;Number of Entries", 30, -3, 3);
  for (std::size_t ii = 0; ii < 500; ++ii)
    h.Fill(gRandom->Gaus(), 2);

  TFile f("output.root", "RECREATE");
  // WriteTObject takes a pointer
  f.WriteTObject(&h);

  return 0;
}
