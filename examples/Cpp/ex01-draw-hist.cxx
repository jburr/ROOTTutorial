#include <TFile.h>
#include <TH1.h>
#include <TCanvas.h>
#include <iostream>

int main() {
  TFile f("input.root");
  // Read the histogram from the file
  TH1* h = nullptr;
  f.GetObject("Histograms/Jets/jetPt", h);
  if (!h) {
    std::cerr << "Failed to retrieve histogram from file!" << std::endl;
    return 1;
  }

  TCanvas canvas;
  h->Draw("E");
  canvas.Print("jetPt.pdf");

  return 0;
}
