import ROOT
h = ROOT.TH1F("h_rand", "Random Number;x;Number of Entries", 30, -3, 3)
for _ in range(500):
  h.Fill(ROOT.gRandom.Gaus(), 2)

f = ROOT.TFile("output.root", "RECREATE")
f.WriteTObject(h)
