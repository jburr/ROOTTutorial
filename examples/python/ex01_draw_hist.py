import ROOT
# Prevent ROOT from rendering the canvas to the screen
ROOT.gROOT.SetBatch(True)
f = ROOT.TFile.Open("input.root")
h = f.Get("Histograms/Jets/jetPt")
canvas = ROOT.TCanvas()
h.SetLineColor(ROOT.kRed)
h.Draw("E")
canvas.Print("jetPt.pdf")
